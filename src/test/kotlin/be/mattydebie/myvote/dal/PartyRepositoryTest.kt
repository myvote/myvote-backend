package be.mattydebie.myvote.dal

import be.mattydebie.myvote.domain.Party
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@DataMongoTest
class PartyRepositoryTest {
    @Autowired
    lateinit var mongo: PartyRepository

    @Test
    fun testSaveParty() {
        val amount = mongo.findAll().size

        val partyInsert = Party(UUID.randomUUID().toString(), "SP.a", "Federaal", mutableListOf())
        mongo.insert(partyInsert)

        assert(mongo.findAll().size == amount + 1)

        val partyPromise = mongo.findById(partyInsert.id)
        assert(partyPromise.isPresent)

        val party = partyPromise.get()
        assert(party.id == partyInsert.id)
        assert(party.name == partyInsert.name)
        assert(party.rang == partyInsert.rang)
        assert(party.children.size == partyInsert.children.size)
    }

    @Test
    fun testAddChildrenToParty() {
        var party = Party(UUID.randomUUID().toString(), "NV-a", "Federaal", mutableListOf())
        mongo.insert(party)

        val amount = mongo.count()

        party = mongo.findById(party.id).get()

        party.children.add(
                Party(UUID.randomUUID().toString(), "NV-a", "Antwerpen", mutableListOf())
        )
        mongo.save(party)


        // size should not have increased
        assert(mongo.count() == amount)

        party = mongo.findById(party.id).get()
        assert(party.children.size == 1)
        assert(party.children.first().name == "NV-a")
        assert(party.children.first().rang == "Antwerpen")
    }
}