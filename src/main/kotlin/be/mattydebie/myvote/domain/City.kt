package be.mattydebie.myvote.domain

import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@Entity
@Table(name = "cities")
class City : AuditModel() {

    @Id
    var id: String = UUID.randomUUID().toString()

    @NotBlank
    @Size(min = 3, max = 100)
    lateinit var name: String
}