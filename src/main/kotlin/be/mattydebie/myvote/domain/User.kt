package be.mattydebie.myvote.domain

import be.mattydebie.myvote.domain.requests.UserCreateRequest
import be.mattydebie.myvote.domain.requests.UserUpdateRequest
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users")
data class User(
        @Id
        var id: String = UUID.randomUUID().toString(),

        var email: String? = null,
        var firstname: String? = null,
        var lastname: String? = null,
        var password: String? = null,

        var admin: Boolean = false,


        @ManyToOne
        @JoinColumn(name = "party_id", nullable = true)
        var party: Party? = null
) : AuditModel() {
    companion object {
        fun fromCreate(user: UserCreateRequest) = User(
                email = user.email,
                firstname = user.firstname,
                lastname = user.lastname,
                admin = user.admin,
                party = null
        )
    }


    fun updateWith(user: UserUpdateRequest, aParty: Party? = null, patch: Boolean = false): User {
        user.firstname?.also { firstname = it }
        user.lastname?.also { lastname = it }
        user.email?.also { email = it }
        user.admin?.also { admin = it }
        party = aParty

        return this;
    }
}
