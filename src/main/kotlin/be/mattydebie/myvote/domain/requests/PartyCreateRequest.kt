package be.mattydebie.myvote.domain.requests

import com.fasterxml.jackson.annotation.JsonProperty

data class PartyCreateRequest(
        val name: String,
        @JsonProperty(value = "city_id") val cityId: String,
        @JsonProperty(value = "parent_id", required = false) val parentId: String? = null
)