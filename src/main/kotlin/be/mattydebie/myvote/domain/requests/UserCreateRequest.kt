package be.mattydebie.myvote.domain.requests

import com.fasterxml.jackson.annotation.JsonProperty

data class UserCreateRequest(
        val firstname: String,
        val lastname: String,
        val email: String,
        val admin: Boolean,
        @JsonProperty(value = "party_id") val partyId: String? = null
)