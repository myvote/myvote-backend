package be.mattydebie.myvote.domain.requests

import be.mattydebie.myvote.domain.City

data class CityCreateRequest (
        var name: String
){

    var id: String? = null

    fun toCity() = City().also { c ->
        c.id = id?: c.id
        c.name = name
    }
}