package be.mattydebie.myvote.domain.requests

import be.mattydebie.myvote.domain.City

data class CityUpdateRequest (
        var name: String? = null
)
