package be.mattydebie.myvote.domain.requests

import com.fasterxml.jackson.annotation.JsonProperty

data class PartyUpdateRequest(
        val name: String? = null,
        @JsonProperty(value = "city_id") val cityId: String? = null,
        @JsonProperty(value = "parent_id", required = false) val parentId: String? = null
)