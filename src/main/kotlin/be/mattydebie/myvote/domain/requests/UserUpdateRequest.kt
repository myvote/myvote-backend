package be.mattydebie.myvote.domain.requests

import com.fasterxml.jackson.annotation.JsonProperty

class UserUpdateRequest(
        var firstname: String? = null,
        var lastname: String? = null,
        var admin: Boolean? = null,
        @JsonProperty(value = "party_id") var partyId: String? = null,
        var email: String? = null
)