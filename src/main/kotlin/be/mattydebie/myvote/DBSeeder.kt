package be.mattydebie.myvote

import be.mattydebie.myvote.dal.CityRepository
import be.mattydebie.myvote.dal.PartyRepository
import be.mattydebie.myvote.dal.UserRepository
import be.mattydebie.myvote.domain.City
import be.mattydebie.myvote.domain.Party
import be.mattydebie.myvote.domain.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class DBSeeder {

    @Autowired
    lateinit var cityRepository: CityRepository
    @Autowired
    lateinit var partyRepository: PartyRepository
    @Autowired
    lateinit var userRepository: UserRepository


    val cities = listOf(
            City().also { it.name = "Federaal" },
            City().also { it.name = "Antwerpen (provincie)" },
            City().also { it.name = "Antwerpen (stad)" },
            City().also { it.name = "Berchem" },
            City().also { it.name = "Deurne" },
            City().also { it.name = "Schoten" }
    )
    private val parties = mutableListOf<Party>()
    private val users = mutableListOf<User>()


    @EventListener
    fun seed(event: ContextRefreshedEvent) {
        println("##########################")
        println("# Seeding  ###############")
        println("##########################")
        deleteAll()
        seedCities()
        seedParties()
        seedAdmin()
    }

    private fun deleteAll() {
        userRepository.deleteAll()
        partyRepository.deleteAll()
        cityRepository.deleteAll()
    }

    private fun seedCities() {
        cityRepository.saveAll(cities)
        println("Seeded Cities")
        cities.forEach {
            print("${it.name}, ")
        }
    }

    private fun seedParties() {
        parties.add(Party().also { it.name = "SP.a"; it.city = cities.get(0) })
        parties.add(Party().also { it.name = "SP.a"; it.city = cities.get(1); it.parent = parties.get(0) })
        parties.add(Party().also { it.name = "SP.a"; it.city = cities.get(2); it.parent = parties.get(1) })
        parties.add(Party().also { it.name = "SP.a"; it.city = cities.get(3); it.parent = parties.get(1) })
        parties.add(Party().also { it.name = "SP.a"; it.city = cities.get(4); it.parent = parties.get(1) })
        parties.add(Party().also { it.name = "SP.a"; it.city = cities.get(5); it.parent = parties.get(1) })

        partyRepository.saveAll(parties);

        println("Seeded Parties")
        parties.forEach {
            print("${it.name} ${it.city.name}, ")
        }
    }

    private fun seedAdmin() {
        users.add(
                User(
                        firstname = "Admin",
                        lastname = "Test",
                        password = "\$2a\$10\$AcB0k9uECJdgcqN9gMExGuHYrmirGtCghmDL5S.VOWMb8Tj/vMapS",
                        admin = true,
                        email = "admin@myvote.be"
                ))

        users.add(
                User(
                        firstname = "Test",
                        lastname = "Doe",
                        password = "\$2a\$10\$AcB0k9uECJdgcqN9gMExGuHYrmirGtCghmDL5S.VOWMb8Tj/vMapS",
                        email = "test@myvote.be",
                        party = parties.get(3)
                ))


        userRepository.saveAll(users);
        println("Seeded users")
        users.forEach { print("${it.email}, ") }
    }

}