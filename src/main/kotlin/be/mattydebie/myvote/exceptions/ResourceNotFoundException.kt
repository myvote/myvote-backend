package be.mattydebie.myvote.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class ResourceNotFoundException(msg: String = "Resource could not be found"): RuntimeException(msg)
