package be.mattydebie.myvote.dal

import be.mattydebie.myvote.domain.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository: JpaRepository<User, String>{
    fun findByEmail(email: String): User?
}