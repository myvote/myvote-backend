package be.mattydebie.myvote.dal

import be.mattydebie.myvote.domain.Party
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PartyRepository: JpaRepository<Party, String>