package be.mattydebie.myvote.dal

import be.mattydebie.myvote.domain.City
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CityRepository: JpaRepository<City, String>
