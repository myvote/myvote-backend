package be.mattydebie.myvote.dal

import be.mattydebie.myvote.domain.requests.UserCreateRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import javax.mail.internet.MimeMessage

@Service
class EmailService(
        @Autowired private val mailSender: JavaMailSender,
        @Autowired private val templateEngine: TemplateEngine
) {

    private fun getContext(vararg vars: Pair<String, String>): Context {
        val context = Context()
        vars.forEach {
            context.setVariable(it.first, it.second)
        }

        return context
    }
    private fun getMail(to: String, subject: String, content: String): MimeMessage {

        val mime = mailSender.createMimeMessage()
        MimeMessageHelper(mime, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, "utf-8").apply {
            setFrom("hello@myvote.be")
            setTo(to)
            setSubject(subject)
            setText(content, true)
        }
        return mime
    }

    fun sendWelcome(user: UserCreateRequest, password: String) {
        val content = templateEngine.process("welcome", getContext(
                "firstname" to user.firstname,
                "email" to user.email,
                "password" to password
        ))

        mailSender.send(getMail(user.email, "Hi there", content))
    }
}