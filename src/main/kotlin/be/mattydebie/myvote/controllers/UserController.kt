package be.mattydebie.myvote.controllers

import be.mattydebie.myvote.dal.EmailService
import be.mattydebie.myvote.dal.PartyRepository
import be.mattydebie.myvote.dal.UserRepository
import be.mattydebie.myvote.domain.User
import be.mattydebie.myvote.domain.requests.UserCreateRequest
import be.mattydebie.myvote.domain.requests.UserUpdateRequest
import be.mattydebie.myvote.exceptions.ResourceNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.*
import org.thymeleaf.context.Context
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/users")
class UserController(
        @Autowired private val userRepository: UserRepository,
        @Autowired private val partyRepository: PartyRepository,
        @Autowired private val emailService: EmailService,
        @Autowired private val bcrypt: BCryptPasswordEncoder
) {

    @GetMapping("")
    fun getUsers(): List<User> {
        return userRepository.findAll();
    }

    @GetMapping("/{userId}")
    fun getUser(@PathVariable userId: String): User {
        return userRepository.findById(userId).orElseThrow { ResourceNotFoundException() }
    }

    @PostMapping("")
    fun createUser(@Valid @RequestBody user: UserCreateRequest): User {
        val u = User.fromCreate(user)
        user.partyId?.also { u.party = partyRepository.findById(it).orElseThrow { ResourceNotFoundException() } }

        val password = UUID.randomUUID().toString().split("-").last()
        u.password = bcrypt.encode(password)

        emailService.sendWelcome(user, password)

        return userRepository.save(u)
    }

    @PutMapping("/{userId}")
    fun updateUser(@PathVariable userId: String, @Valid @RequestBody user: UserUpdateRequest): User {
        val u = userRepository.findById(userId).orElseThrow { ResourceNotFoundException() }

        return userRepository.save(u.updateWith(
                user,
                user.partyId?.let { id -> partyRepository.findById(id).get() }
        ))
    }

    @PatchMapping("/{userId}")
    fun patchUser(@PathVariable userId: String, @Valid @RequestBody user: UserUpdateRequest): User {
        val u = userRepository.findById(userId).orElseThrow { ResourceNotFoundException() }

        return userRepository.save(u.updateWith(
                user,
                user.partyId?.let { id -> partyRepository.findById(id).get() },
                true
        ))
    }

    @DeleteMapping("/{userId}")
    fun deleteUser(@PathVariable userId: String): User {
        val user = userRepository.findById(userId).orElseThrow { ResourceNotFoundException() }
        userRepository.deleteById(user.id)
        return user
    }
}