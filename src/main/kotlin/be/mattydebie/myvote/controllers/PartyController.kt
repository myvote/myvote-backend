package be.mattydebie.myvote.controllers

import be.mattydebie.myvote.dal.CityRepository
import be.mattydebie.myvote.dal.PartyRepository
import be.mattydebie.myvote.domain.City
import be.mattydebie.myvote.domain.Party
import be.mattydebie.myvote.domain.requests.PartyCreateRequest
import be.mattydebie.myvote.domain.requests.PartyUpdateRequest
import be.mattydebie.myvote.exceptions.ResourceNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*
import javax.annotation.Resource
import javax.validation.Valid

@RestController
@RequestMapping("/parties")
class PartyController(@Autowired val partyRepository: PartyRepository, @Autowired val cityRepository: CityRepository) {

    @GetMapping("")
    fun getParties(pageable: Pageable): List<Party> {
        return partyRepository.findAll(/*pageable*/);
    }

    @GetMapping("/{partyId}")
    fun getParty(@PathVariable partyId: String): Party {
        return partyRepository.findById(partyId).orElseThrow { ResourceNotFoundException() }
    }

    @PostMapping("")
    fun createParty(@Valid @RequestBody party: PartyCreateRequest): Party {

        val cityFound = cityRepository.findById(party.cityId).orElseThrow { ResourceNotFoundException() }
        val parentFound =
                if (party.parentId != null)
                    partyRepository.findById(party.parentId).orElseThrow { ResourceNotFoundException() }
                else
                    null

        return partyRepository.save(Party().apply {
            name = party.name
            city = cityFound
            if (parentFound != null) parent = parentFound
        })
    }

    @PutMapping("/{partyId}")
    fun updateParty(@PathVariable partyId: String, @Valid @RequestBody party: PartyCreateRequest): Party {
        val p = partyRepository.findById(partyId).orElseThrow { ResourceNotFoundException() }

        p.name = party.name
        p.city = cityRepository.findById(party.cityId).orElseThrow { ResourceNotFoundException() }

        p.parent = null
        party.parentId?.also { id ->
            if( party.parentId != partyId)
                p.parent = partyRepository.findById(id).orElseThrow { ResourceNotFoundException() }
        }

        return partyRepository.save(p)
    }

    @PatchMapping("/{partyId}")
    fun patchParty(@PathVariable partyId: String, @Valid @RequestBody party: PartyUpdateRequest): Party{
        val p = partyRepository.findById(partyId).orElseThrow { ResourceNotFoundException() }
        p.name = party.name?: p.name

        party.cityId?.also { id ->
            p.city = cityRepository.findById(id).orElseThrow { ResourceNotFoundException() }
        }

        party.parentId?.also { id ->
            if( party.parentId != partyId)
                p.parent = partyRepository.findById(id).orElseThrow { ResourceNotFoundException() }
        }

        return partyRepository.save(p)
    }

    @DeleteMapping("/{partyId}")
    fun deleteParty(@PathVariable partyId: String): Party {
        val p = partyRepository.findById(partyId).orElseThrow { ResourceNotFoundException() }

        partyRepository.deleteById(p.id)
        return p;
    }

}
