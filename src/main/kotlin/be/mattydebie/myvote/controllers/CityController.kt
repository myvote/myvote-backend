package be.mattydebie.myvote.controllers

import be.mattydebie.myvote.dal.CityRepository
import be.mattydebie.myvote.domain.City
import be.mattydebie.myvote.domain.requests.CityCreateRequest
import be.mattydebie.myvote.domain.requests.CityUpdateRequest
import be.mattydebie.myvote.exceptions.ResourceNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.crossstore.ChangeSetPersister
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/cities")
class CityController(@Autowired private val cityRepo: CityRepository) {

    @GetMapping("")
    fun getCities(): List<City> {
        return cityRepo.findAll()
    }

    @GetMapping("/{cityId}")
    fun getCity(@PathVariable cityId: String): City {
        return cityRepo.findById(cityId).orElseThrow { ResourceNotFoundException() }
    }

    @PostMapping("")
    fun createCity(@Valid @RequestBody city: CityCreateRequest): City {
        return cityRepo.save(city.toCity())
    }

    @PutMapping("/{cityId}")
    fun updateCity(@PathVariable cityId: String, @Valid @RequestBody city: CityCreateRequest): City{
        val c = cityRepo.findById(cityId).orElseThrow { ResourceNotFoundException() }

        return cityRepo.save(
                city.toCity().also { it.id = c.id }
        )
    }

    @PatchMapping("/{cityId}")
    fun patchCity(@PathVariable cityId: String, @Valid @RequestBody city: CityUpdateRequest): City{
        val c = cityRepo.findById(cityId).orElseThrow { ResourceNotFoundException() }

        c.name = city.name?: c.name

        return cityRepo.save(c)

    }

    @DeleteMapping("/{cityId}")
    fun deleteCity(@PathVariable cityId: String): City {
        val c = cityRepo.findById(cityId).orElseThrow { ResourceNotFoundException() }

        cityRepo.deleteById(c.id)
        return c;
    }



}