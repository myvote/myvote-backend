package be.mattydebie.myvote

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@SpringBootApplication
class MyvoteApplication{
}

fun main(args: Array<String>) {
    runApplication<MyvoteApplication>(*args)
}


