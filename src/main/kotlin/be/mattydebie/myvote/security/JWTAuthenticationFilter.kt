package be.mattydebie.myvote.security

import be.mattydebie.myvote.security.SecurityConstants.Companion.EXPIRATION_TIME
import be.mattydebie.myvote.security.SecurityConstants.Companion.SECRET
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JWTAuthenticationFilter(authenticationManager: AuthenticationManager) : UsernamePasswordAuthenticationFilter() {

    init {
        this.authenticationManager = authenticationManager
    }


    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        val creds = ObjectMapper().readValue<be.mattydebie.myvote.domain.User>(request.inputStream)
        return authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(creds.email, creds.password, listOf())
        )
    }

    override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain, authResult: Authentication) {
        val tokenBuilder = JWT.create()
                .withSubject((authResult.principal as User).username)
                .withExpiresAt(Date(System.currentTimeMillis() + EXPIRATION_TIME))

        authResult.authorities.forEach { a -> tokenBuilder.withClaim(a.authority, true) }


        response.writer.print(ObjectMapper().writeValueAsString(mapOf("token" to tokenBuilder.sign(Algorithm.HMAC512(SECRET)))))

    }
}