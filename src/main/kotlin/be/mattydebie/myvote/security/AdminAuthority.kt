package be.mattydebie.myvote.security

import org.springframework.security.core.GrantedAuthority

class AdminAuthority: GrantedAuthority{
    override fun getAuthority() = "admin"

}