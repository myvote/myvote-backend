package be.mattydebie.myvote.security

class SecurityConstants {
    companion object {
        val EXPIRATION_TIME = 86_400_000 // one day?
        val SECRET = "supersecretsecret"
        val TOKEN_PREFIX = "Bearer "
        val HEADER_STRING = "Authorization"
    }
}