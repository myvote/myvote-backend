package be.mattydebie.myvote.security

import be.mattydebie.myvote.dal.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CustomUserDetailsService(
        @Autowired private val userRepository: UserRepository
): UserDetailsService{

    override fun loadUserByUsername(email: String): UserDetails {
        val user = userRepository.findByEmail(email) ?: throw UsernameNotFoundException(email)

        val authorities = mutableListOf<GrantedAuthority>()

        if( user.admin ) authorities.add(AdminAuthority())

        return User(user.email, user.password, authorities)
    }
}